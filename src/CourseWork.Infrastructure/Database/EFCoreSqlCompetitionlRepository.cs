﻿using CourseWork.Core.Abstractions.Database;
using CourseWork.Core.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Reflection;

namespace CourseWork.Infrastructure.Database
{
    public class EFCoreSqlCompetitionlRepository : ICompetitionRepository
    {
        private readonly string _connectionString;

        public EFCoreSqlCompetitionlRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public void AddResults(CompetitionResults compResult)
        {
            using (var db = new CompetitionResultsContext(_connectionString))
            {
                db.Results.Add(compResult);
                db.SaveChanges();
            }
        }

        public IReadOnlyCollection<CompetitionResults> GetResults(bool sortType, string sortAttribute, string filterString)
        {
            PropertyInfo? getPropShortened(string attribute) => typeof(CompetitionResults).GetProperty(attribute);

            //TO-DO Maybe refactor? DRY is crying rn because of this:
            using var db = new CompetitionResultsContext(_connectionString);
            return ((sortType) ? db.Results.ToList().OrderBy(result => getPropShortened(sortAttribute)!.GetValue(result, null))
                : db.Results.ToList().OrderByDescending(result => getPropShortened(sortAttribute)!.GetValue(result, null)))
                .Where(result => result.Code.StartsWith(filterString))
                .ToList();
        }

        public CompetitionResults? GetResultsById(ulong id)
        {
            using var db = new CompetitionResultsContext(_connectionString);
            return db.Results.FirstOrDefault(result => result.Id == id);
        }

        public void RemoveResults(CompetitionResults compResult)
        {
            using (var db = new CompetitionResultsContext(_connectionString))
            {
                db.Results.Remove(compResult);
                db.SaveChanges();
            }
        }

        public void RemoveResultsBy(uint condition)
        {
            using (var db= new CompetitionResultsContext(_connectionString))
            {
                db.Results.RemoveRange(db.Results.Where(result => result.WinCount < condition));
                db.SaveChanges();
            }
        }

        public void UpdateResults(CompetitionResults compResult)
        {
            using (var db= new CompetitionResultsContext(_connectionString))
            {
                var result = db.Results.FirstOrDefault(result => result.Id == compResult.Id);
                result!.Code = compResult.Code;
                result.Date = compResult.Date;
                result.WinCount = compResult.WinCount;

                db.SaveChanges();
            }
        }
    }
}
