﻿using CourseWork.Core.Abstractions.Database;
using CourseWork.Core.Entities;
using System.Reflection;

namespace CourseWork.Infrastructure.Database
{
    public class InMemoryCompetitionRepository : ICompetitionRepository
    {
        private readonly List<CompetitionResults> _results = new();

        public InMemoryCompetitionRepository() 
        {
            _results.Add(new CompetitionResults(1, "uss91", new DateTime(1991, 12, 26), 0));
            _results.Add(new CompetitionResults(2, "12FRA", new DateTime(2004, 1, 13), 35));
            _results.Add(new CompetitionResults(3, "GeRm45", new DateTime(2012, 6, 27), 21));
            _results.Add(new CompetitionResults(4, "213ISO", new DateTime(2019, 5, 22), 17));
            _results.Add(new CompetitionResults(5, "IBN51", new DateTime(1978, 7, 17), 5));
            _results.Add(new CompetitionResults(6, "ULTR20", new DateTime(1954, 11, 18), 112));
            _results.Add(new CompetitionResults(7, "OLYM21", new DateTime(1920, 12, 11), 2));
            _results.Add(new CompetitionResults(8, "Ol25", new DateTime(2020, 2, 5), 5));
        }

        public void AddResults(CompetitionResults compResult)
        {
            compResult.Id = (_results.Count == 0) ? 1 : _results.Max(result => result.Id) + 1;
            _results.Add(compResult);
        }

        public IReadOnlyCollection<CompetitionResults> GetResults(bool sortType, string sortAttribute, string filterString)
        {
            PropertyInfo? getPropShortened(string attribute) => typeof(CompetitionResults).GetProperty(attribute);

            return ((sortType) ? _results.OrderBy(result => getPropShortened(sortAttribute)!.GetValue(result, null)) 
                : _results.OrderByDescending(result => getPropShortened(sortAttribute)!.GetValue(result, null)))
                .Where(result => result.Code.StartsWith(filterString))
                .ToList();
        }

        public CompetitionResults? GetResultsById(ulong id)
        {
            return _results.FirstOrDefault(result => result.Id == id);
        }

        public void RemoveResults(CompetitionResults compResult)
        {
            _results.Remove(compResult);
        }

        public void RemoveResultsBy(uint condition)
        {
            _results.RemoveAll(result => result.WinCount < condition);
        }

        public void UpdateResults(CompetitionResults compResult)
        {
            int index = _results.IndexOf(compResult);
            _results[index].Code = compResult.Code;
            _results[index].Date = compResult.Date;
            _results[index].WinCount = compResult.WinCount;
        }
    }
}
