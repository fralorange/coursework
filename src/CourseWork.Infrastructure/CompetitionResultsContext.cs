﻿using CourseWork.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CourseWork.Infrastructure
{
    public class CompetitionResultsContext : DbContext
    {

        private readonly string _connectionString;
        public DbSet<CompetitionResults> Results { get; set; }

        public CompetitionResultsContext(string connectionString)
        {
            _connectionString= connectionString;

            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseNpgsql(_connectionString);
    }
}
