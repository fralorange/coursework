﻿using CourseWork.Core.Abstractions;
using CourseWork.Core.Abstractions.Database;
using CourseWork.Core.Dtos;
using CourseWork.Core.Services;
using CourseWork.Infrastructure.Database;

ICompetitionRepository compRepository = new InMemoryCompetitionRepository();
ICompetitionService compService = new CompetitionService(compRepository);

while (true)
{
    PrintMenu();

    int answer = Convert.ToInt32(Console.ReadLine());

    if (answer == 0)
        break;

    switch (answer)
    {
        case 1: ShowEntireCompetitionResults(); break;
        case 2: AddCompetitionResults(); break;
        case 3: UpdateCompetitionResults(); break;
        case 4: DeleteCompetitionResults(); break;
        default: Console.WriteLine("Выход..."); break;
    }
}

void PrintMenu()
{
    Console.WriteLine();
    Console.WriteLine("1) Показать информацию о соревнованиях");
    Console.WriteLine("2) Добавить информацию о соревновании");
    Console.WriteLine("3) Редактировать информацию о соревновании");
    Console.WriteLine("4) Удалить информацию о соревновании");
    Console.WriteLine("0) Выход");
    Console.Write("Выбери вариант ответа: ");
}

void ShowEntireCompetitionResults()
{
    Console.WriteLine("ID: \tШифр \tПобеды \tДата");
    foreach (var result in compService.ShowResults(true, "Id", ""))
    {
        Console.WriteLine($"{result.Id}: \t{result.Code} \t{result.WinCount} \t{result.Date:dd/MM/yyyy}");
    }
}

void AddCompetitionResults()
{
    CreateCompetitionResultsDto dto = new();
    Console.WriteLine("Введите атрибуты соревнования: ");
    Console.Write("Шифр: ");
    dto.Code = Console.ReadLine() ?? "CODE123";
    Console.Write("Дата (dd/MM/yyyy):");
    dto.Date = Convert.ToDateTime(Console.ReadLine());
    Console.Write("Победы: ");
    dto.WinCount = Convert.ToUInt32(Console.ReadLine());

    compService.AddResults(dto);
}

void UpdateCompetitionResults()
{
    UpdateCompetitionResultsDto dto = new();
    Console.Write("Введите ID объекта, который желаете редактировать: ");
    dto.Id = Convert.ToUInt64(Console.ReadLine());
    Console.Write("Шифр: ");
    dto.Code = Console.ReadLine() ?? "CODE123";
    Console.Write("Дата (dd/MM/yyyy):");
    dto.Date = Convert.ToDateTime(Console.ReadLine());
    Console.Write("Победы: ");
    dto.WinCount = Convert.ToUInt32(Console.ReadLine());

    compService.UpdateResults(dto);
}

void DeleteCompetitionResults()
{
    Console.Write("Введите ID объекта, который желаете удалить: ");
    compService.DeleteResultsById(Convert.ToUInt64(Console.ReadLine()));
}