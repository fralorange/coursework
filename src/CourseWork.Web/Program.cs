using CourseWork.Core.Abstractions;
using CourseWork.Core.Abstractions.Database;
using CourseWork.Core.Services;
using CourseWork.Infrastructure.Database;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

// Implement DI
builder.Services.AddSingleton<ICompetitionRepository, EFCoreSqlCompetitionlRepository>();
builder.Services.AddScoped<ICompetitionService, CompetitionService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
