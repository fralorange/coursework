﻿using CourseWork.Core.Abstractions;
using CourseWork.Core.Dtos;
using CourseWork.Web.Models;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Globalization;

namespace CourseWork.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICompetitionService _compService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ICompetitionService compService, ILogger<HomeController> logger)
        {
            _compService = compService;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index(bool sortType = false, string sortAttribute = "Id", string filterString = "")
        {
            var dtos = _compService.ShowResults(sortType, sortAttribute, filterString);

            return View(viewName: "Index", model: dtos);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(viewName: "Create");
        }

        [HttpPost]
        public IActionResult Create(CreateCompetitionResultsDto dto)
        {
            _compService.AddResults(dto);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(ulong id)
        {
            var dto = _compService.GetResultsById(id);
            return View(viewName: "Edit", model: dto);
        }

        [HttpPost]
        public IActionResult Edit(UpdateCompetitionResultsDto dto)
        {
            _compService.UpdateResults(dto);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(ulong id)
        {
            _compService.DeleteResultsById(id);
            return RedirectToAction("Index");
        }

        public IActionResult DeleteIf(uint deleteCondition)
        {
            _compService.DeleteResultsByWins(deleteCondition);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Export()
        {
            var records = _compService.ExportResults();

            using var memoryStream = new MemoryStream();
            using var writer = new StreamWriter(memoryStream);
            using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);

            await csv.WriteRecordsAsync(records);
            await writer.FlushAsync();

            return new FileStreamResult(new MemoryStream(memoryStream.ToArray()), "text/csv") { FileDownloadName = "ExportedTable.csv" };
        }

        [HttpPost]
        public async Task<IActionResult> Import(IFormFile uploadFile)
        {
            using var reader = new StreamReader(uploadFile.OpenReadStream());
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            var records = csv.GetRecordsAsync<ImportCompetitionResultsDto>();

            await foreach(var record in records)
            {
                _compService.ImportResults(record);
            }

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}