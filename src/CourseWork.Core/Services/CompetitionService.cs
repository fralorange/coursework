﻿using CourseWork.Core.Abstractions;
using CourseWork.Core.Abstractions.Database;
using CourseWork.Core.Dtos;
using CourseWork.Core.Entities;
using CourseWork.Core.Exceptions;

namespace CourseWork.Core.Services
{
    public class CompetitionService : ICompetitionService
    {
        private readonly ICompetitionRepository _repository;

        public CompetitionService(ICompetitionRepository repository)
        {
            _repository = repository;
        }

        public void AddResults(CreateCompetitionResultsDto createCompetitionResultsDto)
        {
            CompetitionResults results = new(createCompetitionResultsDto.Id, createCompetitionResultsDto.Code, createCompetitionResultsDto.Date,
                createCompetitionResultsDto.WinCount);
            _repository.AddResults(results);
        }

        public void DeleteResultsById(ulong id)
        {
            var results = _repository.GetResultsById(id) ?? throw new CompetitionResultsNotFoundException(id);
            _repository.RemoveResults(results);
        }

        public void DeleteResultsByWins(uint wins)
        {
            _repository.RemoveResultsBy(wins);
        }

        public CompetitionResultsDto? GetResultsById(ulong id)
        {
            var results = _repository.GetResultsById(id) ?? throw new CompetitionResultsNotFoundException(id);
            var resultDto = new CompetitionResultsDto
            {
                Id = results!.Id,
                Code = results.Code,
                Date = results.Date,
                WinCount = results.WinCount,
            };
            return resultDto;
        }

        public IReadOnlyCollection<CompetitionResultsListItemDto> ShowResults(bool sortType, string sortAttribute, string filterString)
        {
            var results = _repository.GetResults(sortType, sortAttribute, filterString)
                .Select(result =>
                {
                    return new CompetitionResultsListItemDto
                    {
                        Id = result.Id,
                        Code = result.Code,
                        Date = result.Date,
                        WinCount = result.WinCount,
                    };
                })
                .ToList();
            return results;
        }

        public void UpdateResults(UpdateCompetitionResultsDto updateCompetitionResultsDto)
        {
            var results = _repository.GetResultsById(updateCompetitionResultsDto.Id) 
                ?? throw new CompetitionResultsNotFoundException(updateCompetitionResultsDto.Id);

            results.Code = updateCompetitionResultsDto.Code;
            results.Date = updateCompetitionResultsDto.Date;
            results.WinCount = updateCompetitionResultsDto.WinCount;

            _repository.UpdateResults(results);
        }

        public IReadOnlyCollection<ExportCompetitionResultDto> ExportResults()
        {
            return _repository.GetResults(true, "Id", "")
                .Select(result =>
                {
                    return new ExportCompetitionResultDto
                    {
                        Id = result.Id,
                        Code = result.Code,
                        Date = DateOnly.FromDateTime(result.Date),
                        WinCount = result.WinCount,
                    };
                })
                .ToList();
        }

        public void ImportResults(ImportCompetitionResultsDto dto)
        {
            _repository.AddResults(new CompetitionResults(dto.Code, dto.Date.ToDateTime(TimeOnly.MinValue), dto.WinCount));
        }

    }
}
