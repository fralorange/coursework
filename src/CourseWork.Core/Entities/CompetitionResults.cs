﻿namespace CourseWork.Core.Entities
{
    public class CompetitionResults
    {
        private ulong _id;
        private string _code;
        private DateTime _date;
        private uint _winCount;

        private CompetitionResults() { }

        public CompetitionResults(string code, DateTime date, uint winCount)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentException(nameof(Code));
            _code = code;
            _date = date;
            _winCount = winCount;
        }

        public CompetitionResults(ulong id, string code, DateTime date, uint winCount)
            : this(code, date, winCount)
        {
            Id = id;
        }

        public ulong Id
        {
            set => _id = value;
            get => _id;
        }

        public string Code
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException(nameof(Code));
                _code = value;
            }
            get => _code;

        }

        public DateTime Date
        {
            set => _date = value;
            get => _date;
        }

        public uint WinCount
        {
            set => _winCount = value;
            get => _winCount;
        }

        public override bool Equals(object? obj)
        {
            if (this == obj)
                return true;

            if (obj == null)
                return false;

            if (obj is not CompetitionResults item)
                return false;

            return Id == item.Id
                && Code == item.Code
                && Date.Equals(item.Date)
                && WinCount == item.WinCount;
        }
    }
}
