﻿namespace CourseWork.Core.Exceptions
{
    public class CompetitionResultsNotFoundException : Exception
    {
        public CompetitionResultsNotFoundException(ulong id)
            : base($"Competition Results with Id: {id} not found!") 
        { }
    }
}
