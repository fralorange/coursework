﻿using CourseWork.Core.Entities;

namespace CourseWork.Core.Abstractions.Database
{
    public interface ICompetitionRepository
    {
        IReadOnlyCollection<CompetitionResults> GetResults(bool sortType, string sortAttribute, string filterString);

        CompetitionResults? GetResultsById(ulong id);

        void AddResults(CompetitionResults compResult);
        void UpdateResults(CompetitionResults compResult);
        void RemoveResults(CompetitionResults compResult);
        void RemoveResultsBy(uint condition);
        
    }
}
