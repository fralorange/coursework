﻿using CourseWork.Core.Dtos;

namespace CourseWork.Core.Abstractions
{
    public interface ICompetitionService
    {
        IReadOnlyCollection<CompetitionResultsListItemDto> ShowResults(bool sortType, string sortAttribute, string filterString);

        void AddResults(CreateCompetitionResultsDto createCompetitionResultsDto);
        void UpdateResults(UpdateCompetitionResultsDto updateCompetitionResultsDto);
        void DeleteResultsById(ulong id);
        void DeleteResultsByWins(uint wins);
        CompetitionResultsDto? GetResultsById(ulong id);
        IReadOnlyCollection<ExportCompetitionResultDto> ExportResults();
        void ImportResults(ImportCompetitionResultsDto importCompetitionResultsDto);
    }
}
