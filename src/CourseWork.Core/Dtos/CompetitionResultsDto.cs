﻿using Microsoft.AspNetCore.Mvc;

namespace CourseWork.Core.Dtos
{
    public class CompetitionResultsDto
    {
        public ulong Id { get; set; }
        public string Code { get; set; } = string.Empty;
        [BindProperty]
        public DateTime Date { get; set; }
        public uint WinCount { get; set; }
    }
}
