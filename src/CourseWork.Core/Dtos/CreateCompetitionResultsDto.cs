﻿namespace CourseWork.Core.Dtos
{
    public class CreateCompetitionResultsDto
    {
        public ulong Id { get; set; }
        public string Code { get; set; }
        public DateTime Date { get; set; }
        public uint WinCount { get; set; }
    }
}
