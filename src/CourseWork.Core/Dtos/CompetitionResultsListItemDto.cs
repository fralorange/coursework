﻿namespace CourseWork.Core.Dtos
{
    public class CompetitionResultsListItemDto
    {
        public ulong Id { get; set; }
        public string Code { get; set; } = string.Empty;
        public DateTime Date { get; set; }
        public uint WinCount { get; set; }
    }
}
