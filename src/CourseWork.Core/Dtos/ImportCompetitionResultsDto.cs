﻿namespace CourseWork.Core.Dtos
{
    public class ImportCompetitionResultsDto
    {
        public string Code { get; set; } = string.Empty;
        public DateOnly Date { get; set; }
        public uint WinCount { get; set; }
    }
}
