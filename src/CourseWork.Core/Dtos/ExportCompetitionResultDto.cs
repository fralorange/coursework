﻿namespace CourseWork.Core.Dtos
{
    public class ExportCompetitionResultDto
    {
        public ulong Id { get; set; }
        public string Code { get; set; } = string.Empty;
        public DateOnly Date { get; set; }
        public uint WinCount { get; set; }
    }
}
