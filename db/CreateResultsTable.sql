CREATE TABLE public."Results"
(
	"Id" serial,
	"Code" text NOT NULL,
	"Date" date NOT NULL,
	"WinCount" numeric CHECK("WinCount" >= 0),
	CONSTRAINT "PK_Results" PRIMARY KEY ("Id")
)